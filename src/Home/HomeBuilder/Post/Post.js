import React, {useEffect, useState} from 'react';
import axiosApi from "../../../axiosApi";
import Button from "../../../Components/Button/Button";

const Post = ({match, history}) => {
    const [blog, setBlog] = useState([]);
    useEffect(() => {
        const fetchData = async() => {
            const response = await axiosApi.get( '/post/' + match.params.blogId + '.json');
            setBlog([response.data])
        }

        fetchData().catch(e => console.log(e));
    }, [match.params.blogId]);

    const onDelete = async e => {
        try {
            await axiosApi.delete( '/post/' + match.params.blogId + '.json');
        } finally {
            history.replace('/');
        }
    }

    const onChange = () => {
        history.replace('/post/' + match.params.blogId + '/edit');
        console.log('/post/' + match.params.blogId + '/edit')
    }
    return (
        <div>
            {blog && blog.map(blog=>(<div key={blog.id}><span>{blog.dateTime}</span><h2>{blog.title}</h2><p>{blog.description}</p><Button name='Change' click={onChange}/><Button name='Delete' click={onDelete}/></div>))}
        </div>
    );
};

export default Post;