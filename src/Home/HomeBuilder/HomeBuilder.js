import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import Button from "../../Components/Button/Button";

const HomeBuilder = ({match, history}) => {
    const [blog, setBlog] = useState([]);
    useEffect(() => {
        const fetchData = async() => {
            const response = await axiosApi.get( '/post/.json');
            setBlog(response.data)
        }
        fetchData().catch(e => console.log(e));
    },[match.params]);

    const blogs = () => {
        const arr = [];
        for(const key in blog){
            arr.push(blog[key])
        }
        return arr;
    }
    const changeSide = index => {
        const key = Object.keys(blog)
        history.replace('/post/' + key[index])
    }

    return (
        <div className="home">
            {blogs().map(b => (<div key={b.id}><span>It was realised:{b.dateTime}</span><h4>{b.title}</h4> <Button name='Read more' click={() => changeSide(blogs().indexOf(b))}/></div>))}
            <h3>My blogs</h3>
        </div>
    );
};

export default HomeBuilder;