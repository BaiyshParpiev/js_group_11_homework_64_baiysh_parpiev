import axios from 'axios';

const axiosApi = axios.create({
    baseURL: 'https://baiysh-parpiev-default-rtdb.firebaseio.com',
});

export default axiosApi;