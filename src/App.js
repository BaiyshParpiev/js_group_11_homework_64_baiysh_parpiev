import React from 'react';
import {BrowserRouter, NavLink, Route, Switch} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import HomeBuilder from "./Home/HomeBuilder/HomeBuilder";
import ContactBuilder from "./Contact/ContactBuilder/ContactBuilder";
import AddBuilder from "./Add/AdddBuilder/AddBuilder";
import Post from "./Home/HomeBuilder/Post/Post";
import Edit from "./Add/Edit/Edit";
import About from "./About/About";

const App = () => {

  return (
      <BrowserRouter>
        <div className="App">
          <header className="App-header">
            <div className="container header">
              <a href="https://shop.mango.com/" className="header__logo">
                <img src={logo} className="App-logo" alt="logo" />
              </a>
              <div className="header__links">
                <ul>
                  <NavLink to="/">Home</NavLink>
                  <NavLink to="/add">Add</NavLink>
                  <NavLink to="/contact">Contact</NavLink>
                  <NavLink to="/about">About</NavLink>
                </ul>
              </div>
            </div>
          </header>
          <div className="block container">
            <Switch>
              <Route path="/" exact component={HomeBuilder}/>
              <Route path="/add"  component={AddBuilder}/>
              <Route path="/contact" component={ContactBuilder}/>
              <Route path="/about" component={About}/>
              <Route path="/post/:blogId/edit" component={Edit}/>
              <Route path="/post/:blogId" component={Post}/>
              <Route render={() => <h1>Not found</h1>}/>
            </Switch>
          </div>
          <footer className="footer">
            <div className="container">
              <div className="footer__top row">
                <div className="footer__mail ">
                  <a href="https//:bayish.parpiev@gmail.com" className="info">baiysh.parpiev@gmail.com</a>
                </div>
                <div className="footer__phone">
                  <a href="https//:instagram.com" className="info">(+49) 1722962698</a>
                  <span className="footer__border"/>
                </div>
              </div>
              <div className="footer__bottom">
                <p className="web-info">Copyright(c) website name. <span className="web-info__text">Designed by: www.alltemplateneeds.com</span> /
                  Images from: www.wallpaperswide.com, www.photorack.net</p>
              </div>
            </div>
          </footer>
        </div>

      </BrowserRouter>
  )
}



export default App;
