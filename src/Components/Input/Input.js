import React from 'react';
import Button from "../Button/Button";

const Input = ({onSubmit, change, post}) => {
    return (
        <form onSubmit={onSubmit}>
            <div className="input-group mb-3 mt-4 pt-4">
                <div className="input-group-prepend">
                    <span className="input-group-text" id="inputGroup-sizing-default">Title</span>
                </div>
                <input type="text" name='title' className="form-control" onChange={change}
                       aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default" value={post.title}/>
            </div>
            <div className="input-group">
                <div className="input-group-prepend">
                    <span className="input-group-text">Description</span>
                </div>
                <textarea className="form-control" name='description' onChange={change} aria-label="With textarea" value={post.description}/>
            </div>
            <Button name='Submit'/>
        </form>
    );
};

export default Input;