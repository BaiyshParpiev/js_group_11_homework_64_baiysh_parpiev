import React, {useState} from 'react';
import './AddBuilder.css'
import dayjs from "dayjs";
import axiosApi from "../../axiosApi";
import Input from "../../Components/Input/Input";

const AddBuilder = ({history}) => {
    const [post, setPost] = useState([]);

    const change = e => {
        const {name, value} = e.target;
        setPost(prev => ({
            ...prev,
            id: dayjs().format(),
            dateTime: dayjs().format(),
            [name]: value,
        }));
    }

    const onSubmit = async e => {
        e.preventDefault();
        try {
            await axiosApi.post('/post.json', {
                ...post,
            });
        } finally {
            history.replace('/');
        }
    }
    return (
        <Input change={change} onSubmit={onSubmit} post={post}/>
    );
};

export default AddBuilder;


