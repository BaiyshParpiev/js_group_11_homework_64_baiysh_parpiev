import React, {useEffect, useState} from 'react';
import dayjs from "dayjs";
import axiosApi from "../../axiosApi";
import Input from "../../Components/Input/Input";

const Edit = ({match, history}) =>{
    const [post, setPost] = useState([]);

    const change = e => {
        const {name, value} = e.target;
        setPost(prev => ({
            ...prev,
            id: dayjs().format(),
            dateTime: dayjs().format(),
            [name]: value,
        }));
    }

    useEffect(() => {
        const fetchData = async() => {
            const response = await axiosApi.get( '/post/' + match.params.blogId + '.json');
            setPost([response.data]);
        }

        fetchData().catch(e => console.log(e));
    }, [match.params.blogId]);

    const onSubmit = async e => {
        e.preventDefault();
        try {
            await axiosApi.put('/post/' + match.params.blogId + '.json', {
                ...post,
            });
        } finally {
            history.replace('/');
        }
    }
    return (
        <div>{post.map(p => (<Input post={p} onSubmit={onSubmit} change={change}/>))}</div>
    )

};

export default Edit;